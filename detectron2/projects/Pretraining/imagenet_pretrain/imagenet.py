import io
import logging
import contextlib
import os
import numpy as np

import torch

from detectron2.data.catalog import MetadataCatalog, DatasetCatalog

"""
This file contains functions to parse COCO-format annotations into dicts in "Detectron2 format".
"""


logger = logging.getLogger(__name__)

__all__ = ["load_imgnet_json"]

class ImageNetAPI:
    def __init__(self, data_root):
        # self.thing_classes = ["Container", "Oil", "Carrier", "ETC"]

        self.train_path = os.path.join(data_root, 'train')
        self.test_path = os.path.join(data_root, 'test')

    @staticmethod
    def cvt_imgnet_to_detectron(class_idx: list, num_classes: int):
        """

        :param (list) class_idices: an array of shape (N, 1) with class index
        :return: (numpy.ndarray) an array of shape (N, 1000) with one hot encoding
        """

        labels = torch.tensor(class_idx)
        return labels

def load_imgnet_test_json(data_root, dataset_name=None, extra_annotation_keys=None):
    with contextlib.redirect_stdout(io.StringIO()):
        img_api = ImageNetAPI(data_root)
        test_path = img_api.test_path

    # meta = MetadataCatalog.get(dataset_name)
    # meta.thing_classes = img_api.thing_classes

    dataset_dicts = []
    class_names = os.listdir(test_path)
    class_names.sort()
    for class_idx in range(len(class_names)):
        class_name = class_names[class_idx]
        class_path = os.path.join(test_path, class_name)
        file_names = os.listdir(class_path)

        for file_name in file_names:
            dataset_dicts.append({'file_name': os.path.join(class_path, file_name), 'class': img_api.cvt_imgnet_to_detectron([class_idx], 1000)})

    logger.info("Loaded {} images in imgnet format from {}".format(len(dataset_dicts), data_root))

    return dataset_dicts


def load_imgnet_json(data_root, dataset_name=None, extra_annotation_keys=None):
    with contextlib.redirect_stdout(io.StringIO()):
        img_api = ImageNetAPI(data_root)
        train_path = img_api.train_path

    # meta = MetadataCatalog.get(dataset_name)
    # meta.thing_classes = img_api.thing_classes

    dataset_dicts = []
    class_names = os.listdir(train_path)
    class_names.sort()
    for class_idx in range(len(class_names)):
        class_name = class_names[class_idx]
        class_path = os.path.join(train_path, class_name)
        file_names = os.listdir(class_path)

        for file_name in file_names:
            dataset_dicts.append({'file_name': os.path.join(class_path, file_name), 'class': img_api.cvt_imgnet_to_detectron([class_idx], 1000)})

    logger.info("Loaded {} images in imgnet format from {}".format(len(dataset_dicts), data_root))

    return dataset_dicts

def register_imgnet_instance(name, data_root):
    DatasetCatalog.register(name, lambda: load_imgnet_json(data_root, name))

def register_imgnet_test_instance(name, data_root):
    DatasetCatalog.register(name, lambda: load_imgnet_test_json(data_root, name))

register_imgnet_instance("imgnet", '/ws/data/')
register_imgnet_test_instance("imgnet_test", '/ws/data/')
