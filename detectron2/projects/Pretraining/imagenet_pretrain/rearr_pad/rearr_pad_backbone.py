# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import fvcore.nn.weight_init as weight_init
import torch
import torch.nn.functional as F
from torch import nn

from detectron2.layers import (
    Conv2d,
    FrozenBatchNorm2d,
    ShapeSpec,
    get_norm,
)

from detectron2.layers import FrozenBatchNorm2d, get_norm, ShapeSpec
from detectron2.modeling import ResNetBlockBase, make_stage, ResNet
from detectron2.modeling.backbone.resnet import BasicStem, BottleneckBlock

from .rearr_pad_conv import RPConv, RFOConv, RFOCConv
from .rearr_pad_net import RPNet

__all__ = ["RPBottleneckBlock", "build_rpnet_backbone_pretrain"]

def c2_xavier_fill(weight: nn.Parameter) -> None:
    """
    Initialize `module.weight` using the "XavierFill" implemented in Caffe2.
    Also initializes `module.bias` to 0.
    Args:
        module (torch.nn.Module): module to initialize.
    """
    # Caffe2 implementation of XavierFill in fact
    # corresponds to kaiming_uniform_ in PyTorch
    nn.init.kaiming_uniform_(weight, a=1)  # pyre-ignore

def c2_msra_fill(weight: nn.Parameter) -> None:
    """
    Initialize `module.weight` using the "MSRAFill" implemented in Caffe2.
    Also initializes `module.bias` to 0.
    Args:
        module (torch.nn.Module): module to initialize.
    """
    # pyre-ignore
    nn.init.kaiming_normal_(weight, mode="fan_out", nonlinearity="relu")


thres = 1
class CustomAct(torch.autograd.Function):
    @staticmethod
    def forward(ctx, input):
        ctx.save_for_backward(input)

        top_mask = input > thres
        mid_mask = (input <= thres) & (input > -thres)
        bot_mask = input <= -thres

        return top_mask * (torch.log(input.clamp(min=thres)) + thres) + mid_mask * input + bot_mask * (-torch.log(-(input.clamp(max=-thres))) - thres)

    @staticmethod
    def backward(ctx, grad_output):
        input, = ctx.saved_tensors

        top_mask = input > thres
        bot_mask = input < -thres

        grad_input = grad_output.clone()
        grad_input[top_mask] = grad_input[top_mask] / input[top_mask]
        grad_input[bot_mask] = grad_input[bot_mask] / -input[bot_mask]

        return grad_input

cact = CustomAct.apply

class OffsetDirectionConv(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        groups,
        stride=1,
        padding=0,
        bias=True,
        dilation=1,
    ):
        super(OffsetDirectionConv, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = _pair(kernel_size)
        self.stride = _pair(stride)
        self.groups = groups
        self.with_bias = bias
        self.padding = padding
        self.dilation = dilation

        self.weight = nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
        if bias:
            self.bias = nn.Parameter(torch.Tensor(out_channels))

    def forward(self, x_in):
        weight_up = self.weight
        weight_down = self.weight.rot90(2, [2, 3])
        weight_right = self.weight.rot90(1, [2, 3])
        weight_left = self.weight.rot90(3, [2, 3])

        x_u = F.conv2d(x_in, weight=weight_up, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)
        x_d = F.conv2d(x_in, weight=weight_down, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)
        x_r = F.conv2d(x_in, weight=weight_right, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)
        x_l = F.conv2d(x_in, weight=weight_left, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)

        x_v = torch.log_softmax(torch.stack([x_u, x_d], dim=4), dim=4)
        x_h = torch.log_softmax(torch.stack([x_r, x_l], dim=4), dim=4)

        val_v, idx_v = torch.max(x_v, dim=4)
        val_h, idx_h = torch.max(x_h, dim=4)

        negative_idx_v = idx_v == 1
        negative_idx_h = idx_h == 1

        val_v[negative_idx_v] = -val_v[negative_idx_v]
        val_h[negative_idx_h] = -val_h[negative_idx_h]

        return torch.cat([val_v, val_h], dim=1)

    def extra_repr(self):
        tmpstr = "in_channels=" + str(self.in_channels)
        tmpstr += ", out_channels=" + str(self.out_channels)
        tmpstr += ", kernel_size=" + str(self.kernel_size)
        tmpstr += ", stride=" + str(self.stride)
        tmpstr += ", padding=" + str(self.padding)
        tmpstr += ", dilation=" + str(self.dilation)
        tmpstr += ", groups=" + str(self.groups)
        tmpstr += ", bias=" + str(self.with_bias)
        return tmpstr


class NorBottleneckBlock(ResNetBlockBase):
    def __init__(
        self,
        in_channels,
        out_channels,
        *,
        bottleneck_channels,
        stride=1,
        num_groups=1,
        norm="BN",
        stride_in_1x1=False,
        dilation=1,
    ):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
            stride_in_1x1 (bool): when stride==2, whether to put stride in the
                first 1x1 convolution or the bottleneck 3x3 convolution.
        """
        super().__init__(in_channels, out_channels, stride)

        if in_channels != out_channels:
            self.shortcut = Conv2d(
                in_channels,
                out_channels,
                kernel_size=1,
                stride=stride,
                bias=False,
                norm=get_norm(norm, out_channels),
            )
        else:
            self.shortcut = None

        # The original MSRA ResNet models have stride in the first 1x1 conv
        # The subsequent fb.torch.resnet and Caffe2 ResNe[X]t implementations have
        # stride in the 3x3 conv
        stride_1x1, stride_3x3 = (stride, 1) if stride_in_1x1 else (1, stride)

        self.conv1 = Conv2d(
            in_channels,
            bottleneck_channels,
            kernel_size=1,
            stride=stride_1x1,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv2 = Conv2d(
            bottleneck_channels,
            bottleneck_channels,
            kernel_size=3,
            stride=stride_3x3,
            padding=1 * dilation,
            bias=False,
            groups=num_groups,
            dilation=dilation,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv3 = Conv2d(
            bottleneck_channels,
            out_channels,
            kernel_size=1,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        for layer in [self.conv1, self.conv2, self.conv3, self.shortcut]:
            if layer is not None:  # shortcut can be None
                weight_init.c2_msra_fill(layer)

        # Zero-initialize the last normalization in each residual branch,
        # so that at the beginning, the residual branch starts with zeros,
        # and each residual block behaves like an identity.
        # See Sec 5.1 in "Accurate, Large Minibatch SGD: Training ImageNet in 1 Hour":
        # "For BN layers, the learnable scaling coefficient γ is initialized
        # to be 1, except for each residual block's last BN
        # where γ is initialized to be 0."

        # nn.init.constant_(self.conv3.norm.weight, 0)
        # TODO this somehow hurts performance when training GN models from scratch.
        # Add it as an option when we need to use this code to train a backbone.

    def forward(self, x):
        out = self.conv1(x)
        out = F.relu_(out)

        out = self.conv2(out)
        out = F.relu_(out)

        out = self.conv3(out)

        if self.shortcut is not None:
            shortcut = self.shortcut(x)
        else:
            shortcut = x

        out += shortcut
        out = F.relu_(out)
        return out

class NorBasicStem(nn.Module):
    def __init__(self, in_channels=3, out_channels=64, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()
        self.conv1 = Conv2d(
            in_channels,
            out_channels,
            kernel_size=7,
            stride=2,
            padding=3,
            bias=False,
            norm=get_norm(norm, out_channels),
        )
        weight_init.c2_msra_fill(self.conv1)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu_(x)
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
        return x

    @property
    def out_channels(self):
        return self.conv1.out_channels

    @property
    def stride(self):
        return 4  # = stride 2 conv -> stride 2 max pool


class RPBottleneckBlock(ResNetBlockBase):
    def __init__(
        self,
        in_channels,
        out_channels,
        *,
        bottleneck_channels,
        stride=1,
        num_groups=1,
        norm="BN",
        stride_in_1x1=False,
        dilation=1,
    ):
        """
        Similar to :class:`BottleneckBlock`, but with deformable conv in the 3x3 convolution.
        """
        super().__init__(in_channels, out_channels, stride)

        rp_conv_op = RPConv
        if in_channels != out_channels:
            self.shortcut = rp_conv_op(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=1,
                # stride=1,
                stride=stride,
                bias=False,
                norm=get_norm(norm, out_channels),
            )
        else:
            self.shortcut = None

        self.stride_1x1, self.stride_3x3 = (stride, 1) if stride_in_1x1 else (1, stride)


        self.conv1_rp = rp_conv_op(
            in_channels=in_channels,
            out_channels=bottleneck_channels,
            kernel_size=1,
            # stride=1,
            stride=self.stride_1x1,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv2_rp = rp_conv_op(
            in_channels=bottleneck_channels,
            out_channels=bottleneck_channels,
            kernel_size=3,
            # stride=1,
            stride=self.stride_3x3,
            bias=False,
            groups=num_groups,
            dilation=dilation,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv3 = Conv2d(
            bottleneck_channels,
            out_channels,
            kernel_size=1,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        for layer in [self.conv1_rp, self.conv2_rp, self.conv3, self.shortcut]:
            if layer is not None:  # shortcut can be None
                weight_init.c2_msra_fill(layer)

    def forward(self, x):
        # out = self.conv1_rp(x, (int(x.shape[2] / self.stride_1x1), int(x.shape[3] / self.stride_1x1)))
        out = self.conv1_rp(x, (int(x.shape[2]), int(x.shape[3])))
        out = F.relu_(out)

        # out = self.conv2_rp(out, (int(out.shape[2] / self.stride_3x3) + 2 * 1, int(out.shape[3] / self.stride_3x3) + 2 * 1))
        out = self.conv2_rp(out, (int(out.shape[2]) + 2 * 1, int(out.shape[3]) + 2 * 1))
        out = F.relu_(out)

        out = self.conv3(out)

        if self.shortcut is not None:
            # shortcut = self.shortcut(x, (int(x.shape[2] / self.stride), int(x.shape[3] / self.stride)))
            shortcut = self.shortcut(x, (int(x.shape[2]), int(x.shape[3])))
        else:
            shortcut = x

        out += shortcut
        out = F.relu_(out)
        return out

class RPStem(nn.Module):
    def __init__(self, in_channels=3, out_channels=64, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()

        rp_conv_op = RPConv

        self.conv1_rp = rp_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=7,
            stride=2,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        weight_init.c2_msra_fill(self.conv1_rp)

    def forward(self, x):
        x = self.conv1_rp(x, (x.shape[2] + 2 * 3, x.shape[3] + 2 * 3))
        x = F.relu_(x)
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
        return x

    @property
    def out_channels(self):
        return self.conv1_rp.out_channels

    @property
    def stride(self):
        return 4  # = stride 2 conv -> stride 2 max pool

class RFOBottleneckBlock(ResNetBlockBase):
    def __init__(
        self,
        in_channels,
        out_channels,
        *,
        bottleneck_channels,
        stride=1,
        num_groups=1,
        norm="BN",
        stride_in_1x1=False,
        dilation=1,
    ):
        """
        Similar to :class:`BottleneckBlock`, but with deformable conv in the 3x3 convolution.
        """
        super().__init__(in_channels, out_channels, stride)

        rfo_conv_op = RFOConv
        if in_channels != out_channels:
            self.shortcut_rfo = rfo_conv_op(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=1,
                offset_std=0.05,
                stride=1,
                bias=False,
                norm=get_norm(norm, out_channels),
            )
        else:
            self.shortcut_rfo = None

        self.stride_1x1, self.stride_3x3 = (stride, 1) if stride_in_1x1 else (1, stride)


        self.conv1_rfo = rfo_conv_op(
            in_channels=in_channels,
            out_channels=bottleneck_channels,
            kernel_size=1,
            offset_std=0.05,
            # stride=1,
            stride=self.stride_1x1,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv2_rfo = rfo_conv_op(
            in_channels=bottleneck_channels,
            out_channels=bottleneck_channels,
            kernel_size=3,
            offset_std=0.05,
            # stride=1,
            stride=self.stride_3x3,
            bias=False,
            groups=num_groups,
            dilation=dilation,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv3 = Conv2d(
            bottleneck_channels,
            out_channels,
            kernel_size=1,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        for layer in [self.conv1_rfo, self.conv2_rfo, self.conv3, self.shortcut_rfo]:
            if layer is not None:  # shortcut can be None
                weight_init.c2_msra_fill(layer)

    def forward(self, x):
        # out = self.conv1_rfo(x, (int(x.shape[2] / self.stride_1x1), int(x.shape[3] / self.stride_1x1)))
        out = self.conv1_rfo(x, (int(x.shape[2]), int(x.shape[3])))
        out = F.relu_(out)

        # out = self.conv2_rfo(out, (int(out.shape[2] / self.stride_3x3) + 2 * 1, int(out.shape[3] / self.stride_3x3) + 2 * 1))
        out = self.conv2_rfo(out, (int(out.shape[2]) + 2 * 1, int(out.shape[3]) + 2 * 1))
        out = F.relu_(out)

        out = self.conv3(out)

        if self.shortcut_rfo is not None:
            # shortcut = self.shortcut_rfo(x, (int(x.shape[2] / self.stride), int(x.shape[3] / self.stride)))
            shortcut = self.shortcut_rfo(x, (int(x.shape[2] / self.stride), int(x.shape[3] / self.stride)))
        else:
            shortcut = x

        out += shortcut
        out = F.relu_(out)
        return out

class RFOStem(nn.Module):
    def __init__(self, in_channels=3, out_channels=64, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()

        rfo_conv_op = RFOConv

        self.conv1_rfo = rfo_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=7,
            offset_std=0.02,
            stride=2,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        weight_init.c2_msra_fill(self.conv1_rfo)

    def forward(self, x):
        x = self.conv1_rfo(x, (x.shape[2] + 2 * 3, x.shape[3] + 2 * 3))
        x = F.relu_(x)

        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
        return x

    @property
    def out_channels(self):
        return self.conv1_rfo.out_channels

    @property
    def stride(self):
        return 4  # = stride 2 conv -> stride 2 max pool

class RFOCBottleneckBlock(ResNetBlockBase):
    def __init__(
        self,
        in_channels,
        out_channels,
        *,
        bottleneck_channels,
        stride=1,
        num_groups=1,
        norm="BN",
        stride_in_1x1=False,
        dilation=1,
    ):
        """
        Similar to :class:`BottleneckBlock`, but with deformable conv in the 3x3 convolution.
        """
        super().__init__(in_channels, out_channels, stride)

        rfoc_conv_op = RFOCConv
        if in_channels != out_channels:
            self.shortcut_rfoc = rfoc_conv_op(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=1,
                offset_scale=None,
                offset_std=0.001,
                stride=1,
                bias=False,
                norm=get_norm(norm, out_channels),
            )
        else:
            self.shortcut_rfoc = None

        self.stride_1x1, self.stride_3x3 = (stride, 1) if stride_in_1x1 else (1, stride)


        self.conv1_rfoc = rfoc_conv_op(
            in_channels=in_channels,
            out_channels=bottleneck_channels,
            kernel_size=1,
            offset_scale=0.1,
            offset_std=0.001,
            # stride=1,
            stride=self.stride_1x1,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv2_rfoc = rfoc_conv_op(
            in_channels=bottleneck_channels,
            out_channels=bottleneck_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.001,
            # stride=1,
            stride=self.stride_3x3,
            bias=False,
            groups=num_groups,
            dilation=dilation,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv3 = Conv2d(
            bottleneck_channels,
            out_channels,
            kernel_size=1,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        for layer in [self.conv1_rfoc, self.conv2_rfoc, self.conv3, self.shortcut_rfoc]:
            if layer is not None:  # shortcut can be None
                weight_init.c2_msra_fill(layer)

    def forward(self, x):
        # out = self.conv1_rfo(x, (int(x.shape[2] / self.stride_1x1), int(x.shape[3] / self.stride_1x1)))
        out = self.conv1_rfoc(x, (int(x.shape[2]), int(x.shape[3])))
        out = F.relu_(out)

        # out = self.conv2_rfo(out, (int(out.shape[2] / self.stride_3x3) + 2 * 1, int(out.shape[3] / self.stride_3x3) + 2 * 1))
        out = self.conv2_rfoc(out, (int(out.shape[2]) + 2 * 1, int(out.shape[3]) + 2 * 1))
        out = F.relu_(out)

        out = self.conv3(out)

        if self.shortcut_rfoc is not None:
            # shortcut = self.shortcut_rfo(x, (int(x.shape[2] / self.stride), int(x.shape[3] / self.stride)))
            shortcut = self.shortcut_rfoc(x, (int(x.shape[2] / self.stride), int(x.shape[3] / self.stride)))
        else:
            shortcut = x


        out += shortcut
        out = F.relu_(out)
        return out

class RFOCStem(nn.Module):
    def __init__(self, in_channels=3, out_channels=64, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()

        rfoc_conv_op = RFOCConv

        self.conv1_rfoc = rfoc_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=7,
            offset_scale=0.1,
            offset_std=0.001,
            stride=2,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        weight_init.c2_msra_fill(self.conv1_rfoc)

    def forward(self, x):
        x = self.conv1_rfoc(x, (x.shape[2] + 2 * 3, x.shape[3] + 2 * 3))

        x = F.relu_(x)
        x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
        return x

    @property
    def out_channels(self):
        return self.conv1_rfoc.out_channels

    @property
    def stride(self):
        return 4  # = stride 2 conv -> stride 2 max pool

def build_rpnet_backbone_pretrain(cfg, input_channels, num_classes):
    """
    Create a ResNet instance from config.

    Returns:
        ResNet: a :class:`ResNet` instance.
    """
    # need registration of new blocks/stems?
    norm = cfg.MODEL.RESNETS.NORM
    # stem = RPStem(
    stem = RFOStem(
    # stem=NorBasicStem(
            in_channels=input_channels,
        out_channels=cfg.MODEL.RESNETS.STEM_OUT_CHANNELS,
        norm=norm,
    )
    freeze_at = cfg.MODEL.BACKBONE.FREEZE_AT

    if freeze_at >= 1:
        for p in stem.parameters():
            p.requires_grad = False
        stem = FrozenBatchNorm2d.convert_frozen_batchnorm(stem)

    # fmt: off
    out_features        = cfg.MODEL.RESNETS.OUT_FEATURES
    depth               = cfg.MODEL.RESNETS.DEPTH
    num_groups          = cfg.MODEL.RESNETS.NUM_GROUPS
    width_per_group     = cfg.MODEL.RESNETS.WIDTH_PER_GROUP
    bottleneck_channels = num_groups * width_per_group
    in_channels         = cfg.MODEL.RESNETS.STEM_OUT_CHANNELS
    out_channels        = cfg.MODEL.RESNETS.RES2_OUT_CHANNELS
    stride_in_1x1       = cfg.MODEL.RESNETS.STRIDE_IN_1X1
    res5_dilation       = cfg.MODEL.RESNETS.RES5_DILATION
    deform_on_per_stage = cfg.MODEL.RESNETS.DEFORM_ON_PER_STAGE
    # fmt: on
    assert res5_dilation in {1, 2}, "res5_dilation cannot be {}.".format(res5_dilation)

    num_blocks_per_stage = {50: [3, 4, 6, 3], 101: [3, 4, 23, 3], 152: [3, 8, 36, 3]}[depth]

    stages = []

    # Avoid creating variables without gradients
    # It consumes extra memory and may cause allreduce to fail
    out_stage_idx = [{"res2": 2, "res3": 3, "res4": 4, "res5": 5}[f] for f in out_features]
    max_stage_idx = max(out_stage_idx)
    for idx, stage_idx in enumerate(range(2, max_stage_idx + 1)):
        dilation = res5_dilation if stage_idx == 5 else 1
        first_stride = 1 if idx == 0 or (stage_idx == 5 and dilation == 2) else 2
        stage_kargs = {
            "num_blocks": num_blocks_per_stage[idx],
            "first_stride": first_stride,
            "in_channels": in_channels,
            "bottleneck_channels": bottleneck_channels,
            "out_channels": out_channels,
            "num_groups": num_groups,
            "norm": norm,
            "stride_in_1x1": stride_in_1x1,
            "dilation": dilation,
        }
        if deform_on_per_stage[idx]:
            # stage_kargs["block_class"] = RPBottleneckBlock
            # stage_kargs["block_class"] = RFOBottleneckBlock
            stage_kargs["block_class"] = RFOCBottleneckBlock
        else:
            stage_kargs["block_class"] = NorBottleneckBlock
        blocks = make_stage(**stage_kargs)
        in_channels = out_channels
        out_channels *= 2
        bottleneck_channels *= 2

        if freeze_at >= stage_idx:
            for block in blocks:
                block.freeze()
        stages.append(blocks)
    return RPNet(stem, stages, num_classes=num_classes, out_features=out_features)
