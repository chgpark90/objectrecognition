# -*- coding: utf-8 -*-
# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved

from detectron2.config import CfgNode as CN


def add_gtnet_config(cfg):
    """
    Add config for grouptridentnet.
    """
    _C = cfg

    _C.MODEL.GTNETS = CN()
    _C.MODEL.GTNETS.TEST_BRANCH_IDX = 1
    _C.MODEL.GTNETS.RES2_OUT_CHANNELS = 256
    _C.MODEL.GTNETS.STEM_OUT_CHANNELS = 64
    _C.MODEL.GTNETS.NORM = "BN"
    _C.MODEL.GTNETS.OUT_FEATURES = ["res2", "res3", "res4", "res5"]
    _C.MODEL.GTNETS.DEPTH = 50
    _C.MODEL.GTNETS.NUM_GROUPS = 1
    _C.MODEL.GTNETS.WIDTH_PER_GROUP = 64
    _C.MODEL.GTNETS.STRIDE_IN_1X1 = False
    _C.MODEL.GTNETS.RES5_DILATION = 1
    _C.MODEL.GTNETS.NUM_BRANCH = 3
    _C.MODEL.GTNETS.BRANCH_DILATIONS = [1, 2, 3]
    _C.MODEL.GTNETS.TEST_BRANCH_IDX = 1
    _C.MODEL.GTNETS.TRIDENT_STAGE = "res4"
    _C.MODEL.GTNETS.BBOX_REG_WEIGHTS = (
    (10.0, 10.0, 5.0, 5.0),
    (20.0, 20.0, 10.0, 10.0),
    (30.0, 30.0, 15.0, 15.0),
    )
    _C.MODEL.GTNETS.IOUS = (0.5, 0.6, 0.7)


