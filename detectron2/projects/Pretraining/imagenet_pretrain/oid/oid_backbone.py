# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import fvcore.nn.weight_init as weight_init
import torch
import torch.nn.functional as F
from torch import nn

from detectron2.layers import (
    Conv2d,
    FrozenBatchNorm2d,
    ShapeSpec,
    get_norm,
)

from detectron2.layers import FrozenBatchNorm2d, get_norm, ShapeSpec
from detectron2.modeling import ResNetBlockBase, make_stage, ResNet
from detectron2.modeling.backbone.resnet import BasicStem, BottleneckBlock

from .oid_conv import OidConv
from .oid_net import OidNet

__all__ = ["OidBottleneckBlock", "build_oidnet_backbone_pretrain"]

def c2_xavier_fill(weight: nn.Parameter) -> None:
    """
    Initialize `module.weight` using the "XavierFill" implemented in Caffe2.
    Also initializes `module.bias` to 0.
    Args:
        module (torch.nn.Module): module to initialize.
    """
    # Caffe2 implementation of XavierFill in fact
    # corresponds to kaiming_uniform_ in PyTorch
    nn.init.kaiming_uniform_(weight, a=1)  # pyre-ignore

def c2_msra_fill(weight: nn.Parameter) -> None:
    """
    Initialize `module.weight` using the "MSRAFill" implemented in Caffe2.
    Also initializes `module.bias` to 0.
    Args:
        module (torch.nn.Module): module to initialize.
    """
    # pyre-ignore
    nn.init.kaiming_normal_(weight, mode="fan_out", nonlinearity="relu")


thres = 1
class CustomAct(torch.autograd.Function):
    @staticmethod
    def forward(ctx, input):
        ctx.save_for_backward(input)

        top_mask = input > thres
        mid_mask = (input <= thres) & (input > -thres)
        bot_mask = input <= -thres

        return top_mask * (torch.log(input.clamp(min=thres)) + thres) + mid_mask * input + bot_mask * (-torch.log(-(input.clamp(max=-thres))) - thres)

    @staticmethod
    def backward(ctx, grad_output):
        input, = ctx.saved_tensors

        top_mask = input > thres
        bot_mask = input < -thres

        grad_input = grad_output.clone()
        grad_input[top_mask] = grad_input[top_mask] / input[top_mask]
        grad_input[bot_mask] = grad_input[bot_mask] / -input[bot_mask]

        return grad_input

cact = CustomAct.apply

def offset_direction_conv(x_in, weight, bias=None, stride=1, padding=0, dilation=1):
    weight_up = weight
    weight_down = weight.rot90(2, [2, 3])
    weight_right = weight.rot90(1, [2, 3])
    weight_left = weight.rot90(3, [2, 3])

    x_u = F.conv2d(x_in, weight=weight_up, bias=bias, stride=stride, padding=padding, dilation=dilation)
    x_d = F.conv2d(x_in, weight=weight_down, bias=bias, stride=stride, padding=padding, dilation=dilation)
    x_r = F.conv2d(x_in, weight=weight_right, bias=bias, stride=stride, padding=padding, dilation=dilation)
    x_l = F.conv2d(x_in, weight=weight_left, bias=bias, stride=stride, padding=padding, dilation=dilation)

    x_v = torch.log_softmax(torch.stack([x_u, x_d], dim=4), dim=4)
    x_h = torch.log_softmax(torch.stack([x_r, x_l], dim=4), dim=4)

    val_v, idx_v = torch.max(x_v, dim=4)
    val_h, idx_h = torch.max(x_h, dim=4)

    negative_idx_v = idx_v == 1
    negative_idx_h = idx_h == 1

    val_v[negative_idx_v] = -val_v[negative_idx_v]
    val_h[negative_idx_h] = -val_h[negative_idx_h]

    return torch.cat([val_v, val_h], dim=1)

class OffsetDirectionConv(nn.Module):
    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        groups,
        stride=1,
        padding=0,
        bias=True,
        dilation=1,
    ):
        super(OffsetDirectionConv, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = _pair(kernel_size)
        self.stride = _pair(stride)
        self.groups = groups
        self.with_bias = bias
        self.padding = padding
        self.dilation = dilation

        self.weight = nn.Parameter(torch.Tensor(out_channels, in_channels, *self.kernel_size))
        if bias:
            self.bias = nn.Parameter(torch.Tensor(out_channels))

    def forward(self, x_in):
        weight_up = self.weight
        weight_down = self.weight.rot90(2, [2, 3])
        weight_right = self.weight.rot90(1, [2, 3])
        weight_left = self.weight.rot90(3, [2, 3])

        x_u = F.conv2d(x_in, weight=weight_up, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)
        x_d = F.conv2d(x_in, weight=weight_down, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)
        x_r = F.conv2d(x_in, weight=weight_right, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)
        x_l = F.conv2d(x_in, weight=weight_left, bias=self.bias, stride=self.stride, padding=self.padding, dilation=self.dilation)

        x_v = torch.log_softmax(torch.stack([x_u, x_d], dim=4), dim=4)
        x_h = torch.log_softmax(torch.stack([x_r, x_l], dim=4), dim=4)

        val_v, idx_v = torch.max(x_v, dim=4)
        val_h, idx_h = torch.max(x_h, dim=4)

        negative_idx_v = idx_v == 1
        negative_idx_h = idx_h == 1

        val_v[negative_idx_v] = -val_v[negative_idx_v]
        val_h[negative_idx_h] = -val_h[negative_idx_h]

        return torch.cat([val_v, val_h], dim=1)

    def extra_repr(self):
        tmpstr = "in_channels=" + str(self.in_channels)
        tmpstr += ", out_channels=" + str(self.out_channels)
        tmpstr += ", kernel_size=" + str(self.kernel_size)
        tmpstr += ", stride=" + str(self.stride)
        tmpstr += ", padding=" + str(self.padding)
        tmpstr += ", dilation=" + str(self.dilation)
        tmpstr += ", groups=" + str(self.groups)
        tmpstr += ", bias=" + str(self.with_bias)
        return tmpstr

class OidStem(nn.Module):
    def __init__(self, in_channels=3, out_channels=64, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()

        deform_conv_op = OidConv

        offset_channels = 2 * in_channels
        self.conv1_offset_std = Conv2d(
            in_channels,
            offset_channels,
            kernel_size=1
        )
        self.conv1_oid = deform_conv_op(
            in_channels,
            out_channels,
            kernel_size=7,
            stride=2,
            padding=3,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        weight_init.c2_msra_fill(self.conv1_oid)
        nn.init.constant_(self.conv1_offset_std.weight, 0)
        nn.init.constant_(self.conv1_offset_std.bias, 0)
        self.offset_rate = 0.001

    def forward(self, x):
        offset = cact(self.conv1_offset_std(x)) * self.offset_rate
        offset = offset * torch.randn_like(offset)

        x = self.conv1_oid(x, offset)
        x = F.relu_(x)
        x = F.avg_pool2d(x, kernel_size=3, stride=2, padding=1)
        return x

    @property
    def out_channels(self):
        return self.conv1_oid.out_channels

    @property
    def stride(self):
        return 4  # = stride 2 conv -> stride 2 max pool


class OidBottleneckBlock(ResNetBlockBase):
    def __init__(
        self,
        in_channels,
        out_channels,
        *,
        bottleneck_channels,
        stride=1,
        num_groups=1,
        norm="BN",
        stride_in_1x1=False,
        dilation=1,
    ):
        """
        Similar to :class:`BottleneckBlock`, but with deformable conv in the 3x3 convolution.
        """
        super().__init__(in_channels, out_channels, stride)

        if in_channels != out_channels:
            self.shortcut = Conv2d(
                in_channels,
                out_channels,
                kernel_size=1,
                stride=stride,
                bias=False,
                norm=get_norm(norm, out_channels),
            )
        else:
            self.shortcut = None

        stride_1x1, stride_3x3 = (stride, 1) if stride_in_1x1 else (1, stride)

        deform_conv_op = OidConv

        offset_channels = 2 * in_channels
        self.conv1_offset_std = Conv2d(
            in_channels,
            offset_channels,
            kernel_size=1
        )
        self.conv1_oid = deform_conv_op(
            in_channels,
            bottleneck_channels,
            kernel_size=1,
            stride=stride_1x1,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        offset_channels = 2 * bottleneck_channels
        self.conv2_offset_std = Conv2d(
            bottleneck_channels,
            offset_channels,
            kernel_size=1
        )
        self.conv2_oid = deform_conv_op(
            bottleneck_channels,
            bottleneck_channels,
            kernel_size=3,
            stride=stride_3x3,
            padding=1 * dilation,
            bias=False,
            groups=num_groups,
            dilation=dilation,
            norm=get_norm(norm, bottleneck_channels),
        )

        offset_channels = 2 * bottleneck_channels
        self.conv3_offset_std = Conv2d(
            bottleneck_channels,
            offset_channels,
            kernel_size=1
        )
        self.conv3_oid = deform_conv_op(
            bottleneck_channels,
            out_channels,
            kernel_size=1,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        for layer in [self.conv1_oid, self.conv2_oid, self.conv3_oid, self.shortcut]:
            if layer is not None:  # shortcut can be None
                weight_init.c2_msra_fill(layer)

        nn.init.constant_(self.conv1_offset_std.weight, 0)
        nn.init.constant_(self.conv1_offset_std.bias, 0)
        nn.init.constant_(self.conv2_offset_std.weight, 0)
        nn.init.constant_(self.conv2_offset_std.bias, 0)
        nn.init.constant_(self.conv3_offset_std.weight, 0)
        nn.init.constant_(self.conv3_offset_std.bias, 0)

        self.offset_rate = 0.001

    def forward(self, x):
        offset = cact(self.conv1_offset_std(x)) * self.offset_rate
        offset = offset * torch.randn_like(offset)
        out = self.conv1_oid(x, offset)
        out = F.relu_(out)

        offset = cact(self.conv2_offset_std(out)) * self.offset_rate
        offset = offset * torch.randn_like(offset)
        out = self.conv2_oid(out, offset)
        out = F.relu_(out)

        offset = cact(self.conv3_offset_std(out)) * self.offset_rate
        offset = offset * torch.randn_like(offset)
        out = self.conv3_oid(out, offset)

        if self.shortcut is not None:
            shortcut = self.shortcut(x)
        else:
            shortcut = x

        out += shortcut
        out = F.relu_(out)
        return out

def build_oidnet_backbone_pretrain(cfg, input_channels, num_classes):
    """
    Create a ResNet instance from config.

    Returns:
        ResNet: a :class:`ResNet` instance.
    """
    # need registration of new blocks/stems?
    norm = cfg.MODEL.RESNETS.NORM
    stem = OidStem(
        in_channels=input_channels,
        out_channels=cfg.MODEL.RESNETS.STEM_OUT_CHANNELS,
        norm=norm,
    )
    freeze_at = cfg.MODEL.BACKBONE.FREEZE_AT

    if freeze_at >= 1:
        for p in stem.parameters():
            p.requires_grad = False
        stem = FrozenBatchNorm2d.convert_frozen_batchnorm(stem)

    # fmt: off
    out_features        = cfg.MODEL.RESNETS.OUT_FEATURES
    depth               = cfg.MODEL.RESNETS.DEPTH
    num_groups          = cfg.MODEL.RESNETS.NUM_GROUPS
    width_per_group     = cfg.MODEL.RESNETS.WIDTH_PER_GROUP
    bottleneck_channels = num_groups * width_per_group
    in_channels         = cfg.MODEL.RESNETS.STEM_OUT_CHANNELS
    out_channels        = cfg.MODEL.RESNETS.RES2_OUT_CHANNELS
    stride_in_1x1       = cfg.MODEL.RESNETS.STRIDE_IN_1X1
    res5_dilation       = cfg.MODEL.RESNETS.RES5_DILATION
    deform_on_per_stage = cfg.MODEL.RESNETS.DEFORM_ON_PER_STAGE
    # fmt: on
    assert res5_dilation in {1, 2}, "res5_dilation cannot be {}.".format(res5_dilation)

    num_blocks_per_stage = {50: [3, 4, 6, 3], 101: [3, 4, 23, 3], 152: [3, 8, 36, 3]}[depth]

    stages = []

    # Avoid creating variables without gradients
    # It consumes extra memory and may cause allreduce to fail
    out_stage_idx = [{"res2": 2, "res3": 3, "res4": 4, "res5": 5}[f] for f in out_features]
    max_stage_idx = max(out_stage_idx)
    for idx, stage_idx in enumerate(range(2, max_stage_idx + 1)):
        dilation = res5_dilation if stage_idx == 5 else 1
        first_stride = 1 if idx == 0 or (stage_idx == 5 and dilation == 2) else 2
        stage_kargs = {
            "num_blocks": num_blocks_per_stage[idx],
            "first_stride": first_stride,
            "in_channels": in_channels,
            "bottleneck_channels": bottleneck_channels,
            "out_channels": out_channels,
            "num_groups": num_groups,
            "norm": norm,
            "stride_in_1x1": stride_in_1x1,
            "dilation": dilation,
        }
        if deform_on_per_stage[idx]:
            stage_kargs["block_class"] = OidBottleneckBlock
        else:
            stage_kargs["block_class"] = BottleneckBlock
        blocks = make_stage(**stage_kargs)
        in_channels = out_channels
        out_channels *= 2
        bottleneck_channels *= 2

        if freeze_at >= stage_idx:
            for block in blocks:
                block.freeze()
        stages.append(blocks)
    return OidNet(stem, stages, num_classes=num_classes, out_features=out_features)
