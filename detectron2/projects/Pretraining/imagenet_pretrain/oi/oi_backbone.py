# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import fvcore.nn.weight_init as weight_init
import torch
import torch.nn.functional as F
from torch import nn

from detectron2.layers import (
    Conv2d,
    FrozenBatchNorm2d,
    ShapeSpec,
    get_norm,
)

from detectron2.layers import FrozenBatchNorm2d, get_norm, ShapeSpec
from detectron2.modeling import ResNetBlockBase, make_stage, ResNet
from detectron2.modeling.backbone.resnet import BasicStem, BottleneckBlock

from .oi_conv import OIConv
from .oi_net import RPNet
from .realloc_conv import RealConv

__all__ = ["OIBottleneckBlock", "build_oinet_backbone_pretrain"]

def c2_xavier_fill(weight: nn.Parameter) -> None:
    """
    Initialize `module.weight` using the "XavierFill" implemented in Caffe2.
    Also initializes `module.bias` to 0.
    Args:
        module (torch.nn.Module): module to initialize.
    """
    # Caffe2 implementation of XavierFill in fact
    # corresponds to kaiming_uniform_ in PyTorch
    nn.init.kaiming_uniform_(weight, a=1)  # pyre-ignore

def c2_msra_fill(weight: nn.Parameter) -> None:
    """
    Initialize `module.weight` using the "MSRAFill" implemented in Caffe2.
    Also initializes `module.bias` to 0.
    Args:
        module (torch.nn.Module): module to initialize.
    """
    # pyre-ignore
    nn.init.kaiming_normal_(weight, mode="fan_out", nonlinearity="relu")


thres = 1
class CustomAct(torch.autograd.Function):
    @staticmethod
    def forward(ctx, input):
        ctx.save_for_backward(input)

        top_mask = input > thres
        mid_mask = (input <= thres) & (input > -thres)
        bot_mask = input <= -thres

        return top_mask * (torch.log(input.clamp(min=thres)) + thres) + mid_mask * input + bot_mask * (-torch.log(-(input.clamp(max=-thres))) - thres)

    @staticmethod
    def backward(ctx, grad_output):
        input, = ctx.saved_tensors

        top_mask = input > thres
        bot_mask = input < -thres

        grad_input = grad_output.clone()
        grad_input[top_mask] = grad_input[top_mask] / input[top_mask]
        grad_input[bot_mask] = grad_input[bot_mask] / -input[bot_mask]

        return grad_input

cact = CustomAct.apply

class OIBottleneckBlock(ResNetBlockBase):
    def __init__(
        self,
        in_channels,
        out_channels,
        *,
        bottleneck_channels,
        stride=1,
        num_groups=1,
        norm="BN",
        stride_in_1x1=False,
        dilation=1,
    ):
        """
        Similar to :class:`BottleneckBlock`, but with deformable conv in the 3x3 convolution.
        """
        super().__init__(in_channels, out_channels, stride)

        oi_conv_op = OIConv
        real_conv_op = RealConv
        if in_channels != out_channels:
            self.shortcut_real = real_conv_op(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=1,
                # offset_scale=0.1,
                offset_std=0.01,
                offset_scale=None,
                # offset_std=None,
                shift=0.5 * (stride - 1),
                dilation=1,
                bias=False,
                norm=get_norm(norm, out_channels),
            )
        else:
            self.shortcut_real = None

        self.stride_1x1, self.stride_3x3 = (stride, 1) if stride_in_1x1 else (1, stride)


        self.conv1_real = real_conv_op(
            in_channels=in_channels,
            out_channels=bottleneck_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=1,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv2_oi = oi_conv_op(
            in_channels=bottleneck_channels,
            out_channels=bottleneck_channels,
            kernel_size=9,
            # offset_scale=0.1,
            # offset_std=0.01,
            offset_scale=None,
            offset_std=None,
            shift=0,
            dilation=1,
            groups=num_groups,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv2 = Conv2d(
            in_channels=bottleneck_channels,
            out_channels=bottleneck_channels,
            kernel_size=1,
            padding=0,
            groups=num_groups,
            bias=False,
            norm=get_norm(norm, bottleneck_channels),
        )

        self.conv3_real = real_conv_op(
            in_channels=bottleneck_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5 * (stride - 1),
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels),
        )

        for layer in [self.conv1_real, self.conv2, self.conv3_real, self.shortcut_real]:
            if layer is not None:  # shortcut can be None
                weight_init.c2_msra_fill(layer)

    def forward(self, x):
        grid_size = (int(x.shape[2] / self.stride), int(x.shape[3] / self.stride))
        out = self.conv1_real(x, (x.shape[2], x.shape[3]))
        out = F.relu_(out)

        out = self.conv2_oi(out, (x.shape[2], x.shape[3]))
        # out = self.conv2(out)
        # out = F.relu_(out)

        out = self.conv3_real(out, grid_size)

        if self.shortcut_real is not None:
            shortcut = self.shortcut_real(x, (int(x.shape[2] / self.stride), int(x.shape[3] / self.stride)))
        else:
            shortcut = x

        out += shortcut
        out = F.relu_(out)
        return out

class OIStem(nn.Module):
    def __init__(self, in_channels=3, out_channels=64, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()

        oi_conv_op = OIConv

        self.conv1_oi = oi_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )

        weight_init.c2_msra_fill(self.conv1_oi)
        weight_init.c2_msra_fill(self.conv2_oi)

    def forward(self, x):
        grid_size = (int(x.shape[2] / 2), int(x.shape[3] / 2))
        # x = self.conv1(x)
        # x = F.relu_(x)
        x = self.conv1_oi(x, grid_size)

        grid_size = (int(x.shape[2] / 2), int(x.shape[3] / 2))
        x = self.conv2_oi(x, grid_size)
        x = F.relu_(x)

        # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
        return x

    @property
    def out_channels(self):
        return self.conv1_oi.out_channels

    @property
    def stride(self):
        return 4  # = stride 2 conv -> stride 2 max pool

class RealStem(nn.Module):
    def __init__(self, in_channels=3, out_channels=64, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()

        real_conv_op = RealConv

        self.conv1_real = real_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )

        weight_init.c2_msra_fill(self.conv1_real)
        weight_init.c2_msra_fill(self.conv2_real)

    def forward(self, x):
        grid_size = (int(x.shape[2] / 2), int(x.shape[3] / 2))
        x = self.conv1_real(x, grid_size)

        grid_size = (int(x.shape[2] / 2), int(x.shape[3] / 2))
        x = self.conv2_real(x, grid_size)
        x = F.relu_(x)

        # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
        return x

    @property
    def out_channels(self):
        return self.conv1_real.out_channels

    @property
    def stride(self):
        return 4  # = stride 2 conv -> stride 2 max pool

def build_oinet_backbone_pretrain(cfg, input_channels, num_classes):
    """
    Create a ResNet instance from config.

    Returns:
        ResNet: a :class:`ResNet` instance.
    """
    # need registration of new blocks/stems?
    norm = cfg.MODEL.RESNETS.NORM
    # stem=OIStem(
    stem=RealStem(
    # stem=BasicStem(
        in_channels=input_channels,
        out_channels=cfg.MODEL.RESNETS.STEM_OUT_CHANNELS,
        norm=norm,
    )
    freeze_at = cfg.MODEL.BACKBONE.FREEZE_AT

    if freeze_at >= 1:
        for p in stem.parameters():
            p.requires_grad = False
        stem = FrozenBatchNorm2d.convert_frozen_batchnorm(stem)

    # fmt: off
    out_features        = cfg.MODEL.RESNETS.OUT_FEATURES
    depth               = cfg.MODEL.RESNETS.DEPTH
    num_groups          = cfg.MODEL.RESNETS.NUM_GROUPS
    width_per_group     = cfg.MODEL.RESNETS.WIDTH_PER_GROUP
    bottleneck_channels = num_groups * width_per_group
    in_channels         = cfg.MODEL.RESNETS.STEM_OUT_CHANNELS
    out_channels        = cfg.MODEL.RESNETS.RES2_OUT_CHANNELS
    stride_in_1x1       = cfg.MODEL.RESNETS.STRIDE_IN_1X1
    res5_dilation       = cfg.MODEL.RESNETS.RES5_DILATION
    deform_on_per_stage = cfg.MODEL.RESNETS.DEFORM_ON_PER_STAGE
    # fmt: on
    assert res5_dilation in {1, 2}, "res5_dilation cannot be {}.".format(res5_dilation)

    num_blocks_per_stage = {50: [3, 4, 6, 3], 101: [3, 4, 23, 3], 152: [3, 8, 36, 3]}[depth]

    stages = []

    # Avoid creating variables without gradients
    # It consumes extra memory and may cause allreduce to fail
    out_stage_idx = [{"res2": 2, "res3": 3, "res4": 4, "res5": 5}[f] for f in out_features]
    max_stage_idx = max(out_stage_idx)
    for idx, stage_idx in enumerate(range(2, max_stage_idx + 1)):
        dilation = res5_dilation if stage_idx == 5 else 1
        first_stride = 1 if idx == 0 or (stage_idx == 5 and dilation == 2) else 2
        stage_kargs = {
            "num_blocks": num_blocks_per_stage[idx],
            "first_stride": first_stride,
            "in_channels": in_channels,
            "bottleneck_channels": bottleneck_channels,
            "out_channels": out_channels,
            "num_groups": num_groups,
            "norm": norm,
            "stride_in_1x1": stride_in_1x1,
            "dilation": dilation,
        }
        if deform_on_per_stage[idx]:
            stage_kargs["block_class"] = OIBottleneckBlock
        else:
            stage_kargs["block_class"] = NorBottleneckBlock
        blocks = make_stage(**stage_kargs)
        in_channels = out_channels
        out_channels *= 2
        bottleneck_channels *= 2

        if freeze_at >= stage_idx:
            for block in blocks:
                block.freeze()
        stages.append(blocks)
    return RPNet(stem, stages, num_classes=num_classes, out_features=out_features)



