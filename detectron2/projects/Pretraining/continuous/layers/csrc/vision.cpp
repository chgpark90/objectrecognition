// Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved

#include <torch/extension.h>
#include "rearrange/rearr_conv.h"
#include "rearrange_padding/rearr_pad_conv.h"
#include "rearrange_feature_offset/rearr_feature_offset_conv.h"
#include "rearrange_feature_oi/rearr_feature_oi_conv.h"
#include "rearrange_feature_realloc/rearr_feature_realloc_conv.h"
#include "oipm/oipm_conv.h"
#include "deformed_feature/deform_feature.h"
#include "orientation_inv_deformable/oid_conv.h"

namespace continuous {

#ifdef WITH_CUDA
extern int get_cudart_version();
#endif

std::string get_cuda_version() {
#ifdef WITH_CUDA
  std::ostringstream oss;

  // copied from
  // https://github.com/pytorch/pytorch/blob/master/aten/src/ATen/cuda/detail/CUDAHooks.cpp#L231
  auto printCudaStyleVersion = [&](int v) {
    oss << (v / 1000) << "." << (v / 10 % 100);
    if (v % 10 != 0) {
      oss << "." << (v % 10);
    }
  };
  printCudaStyleVersion(get_cudart_version());
  return oss.str();
#else
  return std::string("not available");
#endif
}

// similar to
// https://github.com/pytorch/pytorch/blob/master/aten/src/ATen/Version.cpp
std::string get_compiler_version() {
  std::ostringstream ss;
#if defined(__GNUC__)
#ifndef __clang__

#if ((__GNUC__ <= 4) && (__GNUC_MINOR__ <= 8))
#error "GCC >= 4.9 is required!"
#endif

  { ss << "GCC " << __GNUC__ << "." << __GNUC_MINOR__; }
#endif
#endif

#if defined(__clang_major__)
  {
    ss << "clang " << __clang_major__ << "." << __clang_minor__ << "."
       << __clang_patchlevel__;
  }
#endif

#if defined(_MSC_VER)
  { ss << "MSVC " << _MSC_FULL_VER; }
#endif
  return ss.str();
}


PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
  m.def("rearr_conv_forward", &rearr_conv_forward, "rearr_conv_forward");
  m.def(
      "rearr_conv_backward_input",
      &rearr_conv_backward_input,
      "rearr_conv_backward_input");
  m.def(
      "rearr_conv_backward_filter",
      &rearr_conv_backward_filter,
      "rearr_conv_backward_filter");

  m.def("oid_conv_forward", &oid_conv_forward, "oid_conv_forward");
  m.def(
      "oid_conv_backward_input",
      &oid_conv_backward_input,
      "oid_conv_backward_input");
  m.def(
      "oid_conv_backward_filter",
      &oid_conv_backward_filter,
      "oid_conv_backward_filter");

  m.def("rearr_pad_conv_forward", &rearr_pad_conv_forward, "rearr_pad_conv_forward");
  m.def(
      "rearr_pad_conv_backward_input",
      &rearr_pad_conv_backward_input,
      "rearr_pad_conv_backward_input");
  m.def(
      "rearr_pad_conv_backward_filter",
      &rearr_pad_conv_backward_filter,
      "rearr_pad_conv_backward_filter");

  m.def("rearr_feature_offset_conv_forward", &rearr_feature_offset_conv_forward, "rearr_feature_offset_conv_forward");
  m.def(
      "rearr_feature_offset_conv_backward_input",
      &rearr_feature_offset_conv_backward_input,
      "rearr_feature_offset_conv_backward_input");
  m.def(
      "rearr_feature_offset_conv_backward_filter",
      &rearr_feature_offset_conv_backward_filter,
      "rearr_feature_offset_conv_backward_filter");

  m.def("deform_feature_forward", &deform_feature_forward, "deform_feature_forward");
  m.def(
      "deform_feature_backward_input",
      &deform_feature_backward_input,
      "deform_feature_backward_input");
  m.def(
      "deform_feature_backward_filter",
      &deform_feature_backward_filter,
      "deform_feature_backward_filter");

}
}
