import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
import torchvision.datasets as datasets

from imagenet_pretrain import OIConv, RealConv, OIPMConv, GConvF, GConv

from detectron2.layers import FrozenBatchNorm2d, get_norm, ShapeSpec, Conv2d
import fvcore.nn.weight_init as weight_init


class Model(nn.Module):
    def __init__(self, in_channels=1, num_classes=10, norm="BN"):
        """
        Args:
            norm (str or callable): a callable that takes the number of
                channels and return a `nn.Module`, or a pre-defined string
                (one of {"FrozenBN", "BN", "GN"}).
        """
        super().__init__()

        oi_conv_op = OIConv
        oipm_conv_op = OIPMConv
        real_conv_op = RealConv
        gef_conv_op = GConvF
        ge_conv_op = GConv

        out_channels = 20

        self.conv1_oi = oi_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=7,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=in_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=7,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv5_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv6_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv7_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=7,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv8_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv9_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv10_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=7,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv11_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=1,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv12_oi = oi_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=1,
            groups=out_channels,
            bias=False,
            norm=get_norm(norm, out_channels)
        )




        self.conv1_oipm = oipm_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_oipm = oipm_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3_oipm = oipm_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4_oipm = oipm_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv5_oipm = oipm_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv6_oipm = oipm_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv7_oipm = oipm_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=5,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0,
            dilation=2,
            bias=False,
            norm=get_norm(norm, out_channels)
        )




        self.conv1_real = real_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv5_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv6_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv7_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )




        self.conv1_real = real_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv5_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv6_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv7_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            offset_scale=0.1,
            offset_std=0.01,
            # offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )

        self.conv1_o_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_o_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3_o_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4_o_real = real_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=1,
            # offset_scale=0.1,
            offset_std=0.01,
            offset_scale=None,
            # offset_std=None,
            shift=0.5,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )


        self.conv1 = Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2 = Conv2d(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3 = Conv2d(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4 = Conv2d(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv5 = Conv2d(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv6 = Conv2d(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv7 = Conv2d(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv8 = Conv2d(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=0,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )

        self.conv1_gef = gef_conv_op(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_gef = gef_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3_gef = gef_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4_gef = gef_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )

        self.conv1_ge = ge_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv2_ge = ge_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv3_ge = ge_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv4_ge = ge_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv5_ge = ge_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv6_ge = ge_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )
        self.conv7_ge = ge_conv_op(
            in_channels=out_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1,
            dilation=1,
            stride=1,
            bias=False,
            norm=get_norm(norm, out_channels)
        )

        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.linear = nn.Linear(out_channels, num_classes)

        weight_init.c2_msra_fill(self.conv1_oi)
        weight_init.c2_msra_fill(self.conv2_oi)
        weight_init.c2_msra_fill(self.conv3_oi)
        weight_init.c2_msra_fill(self.conv4_oi)
        weight_init.c2_msra_fill(self.conv5_oi)
        weight_init.c2_msra_fill(self.conv6_oi)
        weight_init.c2_msra_fill(self.conv7_oi)
        weight_init.c2_msra_fill(self.conv8_oi)
        weight_init.c2_msra_fill(self.conv9_oi)
        weight_init.c2_msra_fill(self.conv10_oi)
        weight_init.c2_msra_fill(self.conv11_oi)

        # weight_init.c2_msra_fill(self.conv1_oipm)
        # weight_init.c2_msra_fill(self.conv2_oipm)
        # weight_init.c2_msra_fill(self.conv3_oipm)
        # weight_init.c2_msra_fill(self.conv4_oipm)
        # weight_init.c2_msra_fill(self.conv5_oipm)
        # weight_init.c2_msra_fill(self.conv6_oipm)
        # weight_init.c2_msra_fill(self.conv7_oipm)

        weight_init.c2_msra_fill(self.conv1_real)
        weight_init.c2_msra_fill(self.conv2_real)
        weight_init.c2_msra_fill(self.conv3_real)
        weight_init.c2_msra_fill(self.conv4_real)
        weight_init.c2_msra_fill(self.conv5_real)
        weight_init.c2_msra_fill(self.conv6_real)
        weight_init.c2_msra_fill(self.conv7_real)

        weight_init.c2_msra_fill(self.conv1_o_real)
        weight_init.c2_msra_fill(self.conv2_o_real)
        weight_init.c2_msra_fill(self.conv3_o_real)
        weight_init.c2_msra_fill(self.conv4_o_real)

        weight_init.c2_msra_fill(self.conv1)
        weight_init.c2_msra_fill(self.conv2)
        weight_init.c2_msra_fill(self.conv3)
        weight_init.c2_msra_fill(self.conv4)
        weight_init.c2_msra_fill(self.conv5)
        weight_init.c2_msra_fill(self.conv6)
        weight_init.c2_msra_fill(self.conv7)
        weight_init.c2_msra_fill(self.conv8)

    def forward(self, x):
        check = 0
        if check == 0:
            grid_size = (int(x.shape[2] + 2), int(x.shape[2] + 2))
            grid_sizex2 = (int(x.shape[2] * 2), int(x.shape[2] * 2))
            grid_sizex3 = (int(x.shape[2] * 3), int(x.shape[2] * 3))
            x = self.conv1_oi(x, grid_sizex3)
            x = F.relu_(x)
            # x = self.conv2(x)
            x = self.conv2_oi(x, grid_sizex2)
            x = F.relu_(x)
            x = self.conv3_oi(x, grid_size)
            x = F.relu_(x)
            x = self.conv3(x)
            x = self.conv1_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)

            grid_size = (int(x.shape[2] + 2), int(x.shape[2] + 2))
            grid_sizex2 = (int(x.shape[2] * 2), int(x.shape[2] * 2))
            grid_sizex3 = (int(x.shape[2] * 3), int(x.shape[2] * 3))
            x = self.conv4_oi(x, grid_sizex3)
            x = F.relu_(x)
            # x = self.conv4(x)
            x = self.conv5_oi(x, grid_sizex2)
            x = F.relu_(x)
            x = self.conv6_oi(x, grid_size)
            x = F.relu_(x)
            x = self.conv5(x)
            x = self.conv2_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)

            grid_size = (int(x.shape[2] + 2), int(x.shape[2] + 2))
            grid_sizex2 = (int(x.shape[2] * 2), int(x.shape[2] * 2))
            grid_sizex3 = (int(x.shape[2] * 3), int(x.shape[2] * 3))
            x = self.conv7_oi(x, grid_sizex3)
            x = F.relu_(x)
            # x = self.conv6(x)
            x = self.conv8_oi(x, grid_sizex2)
            x = F.relu_(x)
            x = self.conv9_oi(x, grid_size)
            x = F.relu_(x)
            x = self.conv7(x)
            x = self.conv3_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)


            grid_size = (int(x.shape[2] + 2), int(x.shape[2] + 2))
            grid_sizex2 = (int(x.shape[2] * 2), int(x.shape[2] * 2))
            grid_sizex3 = (int(x.shape[2] * 3), int(x.shape[2] * 3))
            x = self.conv10_oi(x, grid_sizex3)
            x = F.relu_(x)
            x = self.conv11_oi(x, grid_sizex2)
            x = F.relu_(x)
            x = self.conv12_oi(x, grid_size)
            x = F.relu_(x)
            x = self.conv8(x)
            x = self.conv4_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
        elif check == 1:
            x = self.conv1_real(x, (int(x.shape[2]), int(x.shape[3])))
            x = F.relu_(x)
            x = self.conv2_real(x, (int(x.shape[2]), int(x.shape[3])))
            x = F.relu_(x)
            x = self.conv1_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
            x = self.conv3_real(x, (int(x.shape[2]), int(x.shape[3])))
            x = F.relu_(x)
            x = self.conv4_real(x, (int(x.shape[2]), int(x.shape[3])))
            x = F.relu_(x)
            x = self.conv2_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
            x = self.conv5_real(x, (int(x.shape[2]), int(x.shape[3])))
            x = F.relu_(x)
            x = self.conv6_real(x, (int(x.shape[2]), int(x.shape[3])))
            x = F.relu_(x)
            x = self.conv3_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
            x = self.conv7_real(x, (int(x.shape[2]), int(x.shape[3])))
            x = F.relu_(x)
            x = self.conv4_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))
            # x = F.max_pool2d(x, kernel_size=3, stride=2, padding=1)
            x = F.relu_(x)
        else:
            x = self.conv1_gef(x)
            x = F.relu_(x)
            out = []
            for idx in range(x.shape[4]):
                out.append(self.conv2_oi(x[:, :, :, :, idx], (int(x.shape[2]), int(x.shape[3]))))
            x = F.relu_(torch.stack(out, dim=4))
            x = torch.mean(x, 4)
            self.conv1_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))

            x = self.conv2_gef(x)
            x = F.relu_(x)
            for idx in range(x.shape[4]):
                out.append(self.conv3_oi(x[:, :, :, :, idx], (int(x.shape[2]), int(x.shape[3]))))
            x = F.relu_(torch.stack(out, dim=4))
            x = torch.mean(x, 4)
            self.conv2_o_real(x, (int(x.shape[2] / 2), int(x.shape[3] / 2)))

            x = self.conv3_gef(x)
            x = F.relu_(x)
            for idx in range(x.shape[4]):
                out.append(self.conv4_oi(x[:, :, :, :, idx], (int(x.shape[2]), int(x.shape[3]))))
            x = F.relu_(torch.stack(out, dim=4))
            x = torch.mean(x, 4)
            self.conv3_o_real(x,(int(x.shape[2] / 2), int(x.shape[3] / 2)))

            x = self.conv4_gef(x)
            x = F.relu_(x)
            for idx in range(x.shape[4]):
                out.append(self.conv5_oi(x[:, :, :, :, idx], (int(x.shape[2]), int(x.shape[3]))))
            x = F.relu_(torch.stack(out, dim=4))
            x = torch.mean(x, 4)
            self.conv4_o_real(x,(int(x.shape[2] / 2), int(x.shape[3] / 2)))

        # out = []
        # for idx in range(x.shape[4]):
        #     out.append(self.conv1_o_real(x[:, :, :, :, idx], (int(x.shape[2] / 2), int(x.shape[3] / 2))))
        # x = torch.stack(out, dim=4)
        x = self.avgpool(x)
        x = x.flatten(1)
        x = self.linear(x)

        return x


def train(train_loader, model, criterion, optimizer, epoch, print_freq):
    batch_time = AverageMeter('Time', ':6.3f')
    data_time = AverageMeter('Data', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix="Epoch: [{}]".format(epoch))

    # switch to train mode
    model.train()

    end = time.time()
    for i, (images, target) in enumerate(train_loader):
        # measure data loading time
        data_time.update(time.time() - end)

        images.cuda()
        target = target.cuda()
        # compute output
        output = model(images)
        loss = criterion(output, target)

        # measure accuracy and record loss
        acc1, acc5 = accuracy(output, target, topk=(1, 5))
        losses.update(loss.item(), images.size(0))
        top1.update(acc1[0], images.size(0))
        top5.update(acc5[0], images.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if i % print_freq == 0:
            progress.display(i)


def validate(val_loader, model, criterion, print_freq):
    batch_time = AverageMeter('Time', ':6.3f')
    losses = AverageMeter('Loss', ':.4e')
    top1 = AverageMeter('Acc@1', ':6.2f')
    top5 = AverageMeter('Acc@5', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [batch_time, losses, top1, top5],
        prefix='Test: ')

    # switch to evaluate mode
    model.eval()

    with torch.no_grad():
        end = time.time()
        for i, (images, target) in enumerate(val_loader):

            images.cuda()
            target = target.cuda()

            # compute output
            output = model(images)
            loss = criterion(output, target)

            # measure accuracy and record loss
            acc1, acc5 = accuracy(output, target, topk=(1, 5))
            losses.update(loss.item(), images.size(0))
            top1.update(acc1[0], images.size(0))
            top5.update(acc5[0], images.size(0))

            # measure elapsed time
            batch_time.update(time.time() - end)
            end = time.time()

            if i % print_freq == 0:
                progress.display(i)

        # TODO: this should also be done with the ProgressMeter
        print(' * Acc@1 {top1.avg:.3f} Acc@5 {top5.avg:.3f}'
              .format(top1=top1, top5=top5))

    return top1.avg

class AverageMeter(object):
    """Computes and stores the average and current value"""
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)


class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'


def adjust_learning_rate(optimizer, epoch, lr):
    """Sets the learning rate to the initial LR decayed by 10 every 30 epochs"""
    lr = lr * (0.1 ** (epoch // 30))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res
import time
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '2'
if __name__ == '__main__':
    batch_size = 256
    lr = 0.001
    epochs = 100
    print_freq = 100

    model = Model(1, 10)
    model = torch.nn.DataParallel(model.cuda())
    criterion = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)


    mnist_data_path = '/ws/data/open_datasets/classification/mnist'
    input_size = 32
    train_dataset = datasets.MNIST(mnist_data_path, train=True, download=True,
                                   transform=transforms.Compose([
                                       transforms.Grayscale(3),
                                       # transforms.RandomRotation(45),
                                       transforms.Grayscale(1),
                                       # transforms.RandomHorizontalFlip(),
                                       # transforms.RandomVerticalFlip(),
                                       transforms.Resize((input_size, input_size)),
                                       transforms.ToTensor(),
                                   ]))
    val_dataset = datasets.MNIST(mnist_data_path, train=False, download=True,
                                 transform=transforms.Compose([
                                     transforms.Grayscale(3),
                                     # transforms.RandomRotation(90),
                                     transforms.Grayscale(1),
                                     # transforms.RandomHorizontalFlip(),
                                     transforms.Resize((input_size, input_size)),
                                     transforms.ToTensor(),
                                 ]))
    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=batch_size, shuffle=True,
        num_workers=1)

    val_loader = torch.utils.data.DataLoader(
        val_dataset,
        batch_size=batch_size, shuffle=False,
        num_workers=1)

    for epoch in range(epochs):
        adjust_learning_rate(optimizer, epoch, lr)

        # train for one epoch
        train(train_loader, model, criterion, optimizer, epoch, print_freq=print_freq)

        # evaluate on validation set
        acc1 = validate(val_loader, model, criterion, print_freq=print_freq)

