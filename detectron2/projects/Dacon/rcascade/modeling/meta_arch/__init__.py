# -*- coding: utf-8 -*-
# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved

# import all the meta_arch, so they will be registered
from .rcnn import CustomizedRCNN, CustomProposalNetwork
