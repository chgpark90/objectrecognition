from . import data
from . import modeling
from . import structures

from .config import add_tridentnet_config