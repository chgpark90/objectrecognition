# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import io
import logging
import contextlib
import os
import datetime
import json
import numpy as np
import cv2
import math

import torch

from PIL import Image

from fvcore.common.timer import Timer
from detectron2.structures import BoxMode, PolygonMasks, Boxes
from fvcore.common.file_io import PathManager, file_lock


from detectron2.data.catalog import MetadataCatalog, DatasetCatalog

"""
This file contains functions to parse COCO-format annotations into dicts in "Detectron2 format".
"""


logger = logging.getLogger(__name__)

__all__ = ["load_dacon_train_json", "load_dacon_test_json"]

class DaconAPI:
    def __init__(self, json_file):
        self.thing_classes = ["Container", "Oil", "Carrier", "ETC"]

        with open(json_file) as f:
            data = json.load(f)
        self.features = data['features']

    @staticmethod
    def cvt_dacon_to_detectron(dacon_bbox: list, patch_size: tuple) -> list:
        """ Processes a coordinate array from a geojson into (cy, cx, height, width, theta) format

        :param (list) coords: an array of shape (N, 8) with 4 corner points of boxes
        :return: (numpy.ndarray) an array of shape (N, 5) with coordinates in proper format
        """
        coord = np.asarray(dacon_bbox)
        pts = np.reshape(coord, (-1, 5)).astype(dtype=np.float32)
        cx = pts[:, 0] * patch_size[0]
        cy = pts[:, 1] * patch_size[1]
        width = pts[:, 2] * patch_size[0]
        height = pts[:, 3] * patch_size[1]
        theta = pts[:, 4] * 180 / math.pi

        if width < height:
            width, height = height, width
            theta += 90.0
        arr = [cx, cy, width, height, theta]

        arr = np.asarray(arr).reshape(-1, 5)
        arr = torch.tensor(arr)
        original_dtype = arr.dtype
        arr = arr.double()

        w = arr[:, 2]
        h = arr[:, 3]
        a = arr[:, 4]
        c = torch.abs(torch.cos(a * math.pi / 180.0))
        s = torch.abs(torch.sin(a * math.pi / 180.0))
        # This basically computes the horizontal bounding rectangle of the rotated box
        new_w = c * w + s * h
        new_h = c * h + s * w

        # convert center to top-left corner
        arr[:, 0] -= new_w / 2.0
        arr[:, 1] -= new_h / 2.0
        # bottom-right corner
        arr[:, 2] = arr[:, 0] + new_w
        arr[:, 3] = arr[:, 1] + new_h

        arr = arr[:, :4].to(dtype=original_dtype)
        arr = arr.numpy()
        return arr

    @staticmethod
    def cvt_dacon_to_detectron_rotated(dacon_bbox: list, patch_size: tuple) -> list:
        """ Processes a coordinate array from a geojson into (cy, cx, height, width, theta) format

        :param (list) coords: an array of shape (N, 8) with 4 corner points of boxes
        :return: (numpy.ndarray) an array of shape (N, 5) with coordinates in proper format
        """
        coord = np.asarray(dacon_bbox)
        pts = np.reshape(coord, (-1, 5)).astype(dtype=np.float32)
        cx = pts[:, 0] * patch_size[0]
        cy = pts[:, 1] * patch_size[1]
        width = pts[:, 2] * patch_size[0]
        height = pts[:, 3] * patch_size[1]
        theta = pts[:, 4] * 180 / math.pi

        if width < height:
            width, height = height, width
            theta += 90.0
        detectron_bbox = [cx, cy, width, height, theta]

        return detectron_bbox

def load_dacon_test_json(json_file, image_root, dataset_name=None, extra_annotation_keys=None):
    timer = Timer()
    json_file = PathManager.get_local_path(json_file)
    with contextlib.redirect_stdout(io.StringIO()):
        dacon_api = DaconAPI(json_file)
        anns = dacon_api.features
    if timer.seconds() > 1:
        logger.info("Loading {} takes {:.2f} seconds.".format(json_file, timer.seconds()))

    meta = MetadataCatalog.get(dataset_name)
    meta.thing_classes = dacon_api.thing_classes

    logger.info("Loaded {} images in dacon format from {}".format(len(anns), json_file))

    dataset_dicts = []

    for ann in anns:
        record = {}
        record["file_name"] = os.path.join(image_root, ann['image_id'])
        dataset_dicts.append(record)

    return dataset_dicts


def load_dacon_train_json(json_file, image_root, dataset_name=None, extra_annotation_keys=None):
    """
    Load a json file with DACON's instances annotation format.
    Currently supports instance detection, instance segmentation,
    and person keypoints annotations.

    Args:
        json_file (str): full path to the json file in dacon instances annotation format.
        image_root (str): the directory where the images in this json file exists.
        dataset_name (str): the name of the dataset (e.g., coco_2017_train).
            If provided, this function will also put "thing_classes" into
            the metadata associated with this dataset.
        extra_annotation_keys (list[str]): list of per-annotation keys that should also be
            loaded into the dataset dict (besides "iscrowd", "bbox", "keypoints",
            "category_id", "segmentation"). The values for these keys will be returned as-is.
            For example, the densepose annotations are loaded in this way.

    Returns:
        list[dict]: a list of dicts in Detectron2 standard format. (See
        `Using Custom Datasets </tutorials/datasets.html>`_ )

    Notes:
        1. This function does not read the image files.
           The results do not have the "image" field.
    """

    timer = Timer()
    json_file = PathManager.get_local_path(json_file)
    with contextlib.redirect_stdout(io.StringIO()):
        dacon_api = DaconAPI(json_file)
        anns = dacon_api.features
    if timer.seconds() > 1:
        logger.info("Loading {} takes {:.2f} seconds.".format(json_file, timer.seconds()))

    meta = MetadataCatalog.get(dataset_name)
    meta.thing_classes = dacon_api.thing_classes


    logger.info("Loaded {} images in dacon format from {}".format(len(anns), json_file))

    dataset_dicts = []

    for ann in anns:
        record = {}
        record["file_name"] = os.path.join(image_root, ann['image_id'])
        record["height"] = ann['height']
        record["width"] = ann['width']
        patch_size = (ann['width'], ann['height'])

        objs = []
        properties = ann['properties']
        for p in properties:
            # Check that the image_id in this annotation is the same as
            # the image_id we're looking at.
            # This fails only when the data parsing logic or the annotation file is buggy.

            # The original COCO valminusminival2014 & minival2014 annotation files
            # actually contains bugs that, together with certain ways of using COCO API,
            # can trigger this assertion.
            obj = {}
            obj["bbox"] = dacon_api.cvt_dacon_to_detectron(p["bounds_imcoords"].split(","), patch_size)
            obj["bbox_mode"] = BoxMode.XYXY_ABS
            obj["category_id"] = int(p["type_id"]) - 1
            objs.append(obj)
        record["annotations"] = objs
        dataset_dicts.append(record)

    return dataset_dicts

def load_dacon_rotated_train_json(json_file, image_root, dataset_name=None, extra_annotation_keys=None):
    """
    Load a json file with DACON's instances annotation format.
    Currently supports instance detection, instance segmentation,
    and person keypoints annotations.

    Args:
        json_file (str): full path to the json file in dacon instances annotation format.
        image_root (str): the directory where the images in this json file exists.
        dataset_name (str): the name of the dataset (e.g., coco_2017_train).
            If provided, this function will also put "thing_classes" into
            the metadata associated with this dataset.
        extra_annotation_keys (list[str]): list of per-annotation keys that should also be
            loaded into the dataset dict (besides "iscrowd", "bbox", "keypoints",
            "category_id", "segmentation"). The values for these keys will be returned as-is.
            For example, the densepose annotations are loaded in this way.

    Returns:
        list[dict]: a list of dicts in Detectron2 standard format. (See
        `Using Custom Datasets </tutorials/datasets.html>`_ )

    Notes:
        1. This function does not read the image files.
           The results do not have the "image" field.
    """

    timer = Timer()
    json_file = PathManager.get_local_path(json_file)
    with contextlib.redirect_stdout(io.StringIO()):
        dacon_api = DaconAPI(json_file)
        anns = dacon_api.features
    if timer.seconds() > 1:
        logger.info("Loading {} takes {:.2f} seconds.".format(json_file, timer.seconds()))

    meta = MetadataCatalog.get(dataset_name)
    meta.thing_classes = dacon_api.thing_classes


    logger.info("Loaded {} images in dacon format from {}".format(len(anns), json_file))

    dataset_dicts = []

    for ann in anns:
        record = {}
        record["file_name"] = os.path.join(image_root, ann['image_id'])
        record["height"] = ann['height']
        record["width"] = ann['width']
        patch_size = (ann['width'], ann['height'])

        objs = []
        properties = ann['properties']
        for p in properties:
            # Check that the image_id in this annotation is the same as
            # the image_id we're looking at.
            # This fails only when the data parsing logic or the annotation file is buggy.

            # The original COCO valminusminival2014 & minival2014 annotation files
            # actually contains bugs that, together with certain ways of using COCO API,
            # can trigger this assertion.
            obj = {}
            obj["bbox"] = dacon_api.cvt_dacon_to_detectron_rotated(p["bounds_imcoords"].split(","), patch_size)
            obj["bbox_mode"] = BoxMode.XYWHA_ABS
            obj["category_id"] = int(p["type_id"]) - 1
            objs.append(obj)
        record["annotations"] = objs
        dataset_dicts.append(record)

    return dataset_dicts


def load_dacon_train_all_json(data_list, data_root, dataset_name=None, extra_annotation_keys=None):
    """
    Load a json file with DACON's instances annotation format.
    Currently supports instance detection, instance segmentation,
    and person keypoints annotations.

    Args:
        json_file (str): full path to the json file in dacon instances annotation format.
        image_root (str): the directory where the images in this json file exists.
        dataset_name (str): the name of the dataset (e.g., coco_2017_train).
            If provided, this function will also put "thing_classes" into
            the metadata associated with this dataset.
        extra_annotation_keys (list[str]): list of per-annotation keys that should also be
            loaded into the dataset dict (besides "iscrowd", "bbox", "keypoints",
            "category_id", "segmentation"). The values for these keys will be returned as-is.
            For example, the densepose annotations are loaded in this way.

    Returns:
        list[dict]: a list of dicts in Detectron2 standard format. (See
        `Using Custom Datasets </tutorials/datasets.html>`_ )

    Notes:
        1. This function does not read the image files.
           The results do not have the "image" field.
    """

    dataset_dicts = []

    for data_name in data_list:
        json_file = os.path.join(data_root, "train_patches_" + data_name, "labels.json")
        image_root = os.path.join(data_root, "train_patches_" + data_name, "images")

        timer = Timer()
        json_file = PathManager.get_local_path(json_file)
        with contextlib.redirect_stdout(io.StringIO()):
            dacon_api = DaconAPI(json_file)
            anns = dacon_api.features
        if timer.seconds() > 1:
            logger.info("Loading {} takes {:.2f} seconds.".format(json_file, timer.seconds()))

        meta = MetadataCatalog.get(dataset_name)
        meta.thing_classes = dacon_api.thing_classes


        logger.info("Loaded {} images in dacon format from {}".format(len(anns), json_file))


        for ann in anns:
            record = {}
            record["file_name"] = os.path.join(image_root, ann['image_id'])
            record["height"] = ann['height']
            record["width"] = ann['width']
            patch_size = (ann['width'], ann['height'])

            objs = []
            properties = ann['properties']
            for p in properties:
                # Check that the image_id in this annotation is the same as
                # the image_id we're looking at.
                # This fails only when the data parsing logic or the annotation file is buggy.

                # The original COCO valminusminival2014 & minival2014 annotation files
                # actually contains bugs that, together with certain ways of using COCO API,
                # can trigger this assertion.
                obj = {}
                obj["bbox"] = dacon_api.cvt_dacon_to_detectron(p["bounds_imcoords"].split(","), patch_size)
                obj["bbox_mode"] = BoxMode.XYXY_ABS
                obj["category_id"] = int(p["type_id"]) - 1
                objs.append(obj)
            record["annotations"] = objs
            dataset_dicts.append(record)

    return dataset_dicts

def load_dacon_rotated_train_all_json(data_list, data_root, dataset_name=None, extra_annotation_keys=None):
    """
    Load a json file with DACON's instances annotation format.
    Currently supports instance detection, instance segmentation,
    and person keypoints annotations.

    Args:
        json_file (str): full path to the json file in dacon instances annotation format.
        image_root (str): the directory where the images in this json file exists.
        dataset_name (str): the name of the dataset (e.g., coco_2017_train).
            If provided, this function will also put "thing_classes" into
            the metadata associated with this dataset.
        extra_annotation_keys (list[str]): list of per-annotation keys that should also be
            loaded into the dataset dict (besides "iscrowd", "bbox", "keypoints",
            "category_id", "segmentation"). The values for these keys will be returned as-is.
            For example, the densepose annotations are loaded in this way.

    Returns:
        list[dict]: a list of dicts in Detectron2 standard format. (See
        `Using Custom Datasets </tutorials/datasets.html>`_ )

    Notes:
        1. This function does not read the image files.
           The results do not have the "image" field.
    """

    dataset_dicts = []

    for data_name in data_list:
        json_file = os.path.join(data_root, "train_patches_" + data_name, "labels.json")
        image_root = os.path.join(data_root, "train_patches_" + data_name, "images")

        timer = Timer()
        json_file = PathManager.get_local_path(json_file)
        with contextlib.redirect_stdout(io.StringIO()):
            dacon_api = DaconAPI(json_file)
            anns = dacon_api.features
        if timer.seconds() > 1:
            logger.info("Loading {} takes {:.2f} seconds.".format(json_file, timer.seconds()))

        meta = MetadataCatalog.get(dataset_name)
        meta.thing_classes = dacon_api.thing_classes


        logger.info("Loaded {} images in dacon format from {}".format(len(anns), json_file))


        for ann in anns:
            record = {}
            record["file_name"] = os.path.join(image_root, ann['image_id'])
            record["height"] = ann['height']
            record["width"] = ann['width']
            patch_size = (ann['width'], ann['height'])

            objs = []
            properties = ann['properties']
            for p in properties:
                # Check that the image_id in this annotation is the same as
                # the image_id we're looking at.
                # This fails only when the data parsing logic or the annotation file is buggy.

                # The original COCO valminusminival2014 & minival2014 annotation files
                # actually contains bugs that, together with certain ways of using COCO API,
                # can trigger this assertion.
                obj = {}
                obj["bbox"] = dacon_api.cvt_dacon_to_detectron_rotated(p["bounds_imcoords"].split(","), patch_size)
                obj["bbox_mode"] = BoxMode.XYWHA_ABS
                obj["category_id"] = int(p["type_id"]) - 1
                objs.append(obj)
            record["annotations"] = objs
            dataset_dicts.append(record)

    return dataset_dicts

def register_dacon_test_instance(name, json_file, image_root):
    DatasetCatalog.register(name, lambda: load_dacon_test_json(json_file, image_root, name))

def register_dacon_train_instance(name, json_file, image_root):
    DatasetCatalog.register(name, lambda: load_dacon_train_json(json_file, image_root, name))

def register_dacon_rotated_train_instance(name, json_file, image_root):
    DatasetCatalog.register(name, lambda: load_dacon_rotated_train_json(json_file, image_root, name))

def register_dacon_train_all_instance(name, data_list, data_root):
    DatasetCatalog.register(name, lambda: load_dacon_train_all_json(data_list, data_root, name))

def register_dacon_rotated_train_all_instance(name, data_list, data_root):
    DatasetCatalog.register(name, lambda: load_dacon_rotated_train_all_json(data_list, data_root, name))

register_dacon_test_instance("dacon_test", '/ws/data/dacon/test_patches_1024/labels.json', "/ws/data/dacon/test_patches_1024/images")
register_dacon_train_instance("dacon_train", '/ws/data/dacon/train_patches_1024/labels.json', "/ws/data/dacon/train_patches_1024/images")
register_dacon_rotated_train_instance("dacon_rotated_train", '/ws/data/dacon/new_train_patches_1024/labels.json', "/ws/data/dacon/new_train_patches_1024/images")
register_dacon_test_instance("dacon_rotated_test", '/ws/data/dacon/test_patches_1024/labels.json', "/ws/data/dacon/test_patches_1024/images")
register_dacon_train_all_instance("dacon_all", ["512", "768", "1024"], '/ws/data')
register_dacon_rotated_train_all_instance("dacon_rotated_all", ["512", "768", "1024"], '/ws/data')

def main(args):
    """
    Test the COCO json dataset loader.

    Usage:
        python -m detectron2.data.datasets.coco \
            path/to/json path/to/image_root dataset_name

        "dataset_name" can be "coco_2014_minival_100", or other
        pre-registered ones
    """
    from detectron2.config import get_cfg
    from detectron2.data import MetadataCatalog
    from detectron2.engine import default_setup
    from detectron2.utils.logger import setup_logger
    from detectron2.utils.visualizer import Visualizer
    from detectron2.data import build_detection_train_loader
    import torchvision.transforms as T

    import detectron2.data.datasets  # noqa # add pre-defined metadata
    import sys

    def setup(args):
        """
        Create configs and perform basic setups.
        """
        cfg = get_cfg()
        cfg.merge_from_file(args.config_file)
        cfg.merge_from_list(args.opts)
        cfg.freeze()
        default_setup(cfg, args)
        return cfg

    cfg = setup(args)
    meta = MetadataCatalog.get("dacon")

    data_loader = build_detection_train_loader(cfg)
    data_loader_iter = iter(data_loader)

    # logger = setup_logger(name=__name__)
    # dicts = load_dacon_json("/media/data/ADD_DET/train/labels.json", "/media/data/ADD_DET/train/images", "dacon")
    # logger.info("Done loading {} samples.".format(len(dicts)))

    cv2.namedWindow("test", cv2.WINDOW_NORMAL)

    for batch_data in data_loader_iter:
        for data in batch_data:
            image = data['image']
            image = T.ToPILImage()(image)
            image = np.asarray(image)
            color = (255, 0, 0)

            instance = data['instances']
            pred_boxes = instance.get('gt_boxes').tensor.cpu().detach().numpy()
            scale = 1
            for idx in range(len(instance)):
                pred_box = pred_boxes[idx]

                # start_pt = (int(pred_box[0]), int(pred_box[1]))
                # end_pt = (int(pred_box[2]), int(pred_box[3]))
                # image = cv2.rectangle(image, start_pt, end_pt, color, 2)
                # image = cv2.putText(image, str(pred_class), (int(pred_box[0]), int(pred_box[3])), cv2.FONT_HERSHEY_SIMPLEX,
                #                     0.5, (0, 255, 0), 1)

                points = cv2.boxPoints(((pred_box[0] * scale, pred_box[1] * scale),
                                        (pred_box[2] * scale, pred_box[3] * scale), pred_box[4])).tolist()
                points.append(points[0])
                points = [(int(x), int(y)) for x, y in points]
                image = cv2.polylines(image, np.int32([points]), True, color, 2)

            cv2.imshow("test", image)
            cv2.waitKey(0)


if __name__ == "__main__":
    from detectron2.engine import default_argument_parser
    args = default_argument_parser().parse_args()
    # args.config_file = '/ws/external/projects/DACON_CASCADE/configs/cascade_rs_rcnn_R_50_FPN_3x.yaml'
    args.config_file = '/ws/external/projects/RSRCNN/configs/Base-RCNN-FPN.yaml'
    main(args)
