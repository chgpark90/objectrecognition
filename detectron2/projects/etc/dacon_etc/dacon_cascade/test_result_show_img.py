import numpy as np
import os
import cv2
from detectron2.layers.nms import nms_rotated
import numpy as np
import os
import cv2

data_root = '/ws/data/dacon/test/images'
result_name = '/ws/external/projects/Dacon_RSCascade/.csv'

txt = np.loadtxt(result_name, dtype=np.str)
result = []
for t in txt:
    result.append(t.split(','))

img_names = []
for r in result[1:]:
    if not r[0] in img_names:
        img_names.append(r[0])

cv2.namedWindow("test", cv2.WINDOW_NORMAL)
color = (255, 0, 0)
for img_name in img_names:
    img_path = os.path.join(data_root, img_name)

    img = cv2.imread(img_path)

    for r in result[1:]:
        if r[0] == img_name:
            pred_class = r[1]
            score = r[2]
            # start_pt = (int(float(r[3])), int(float(r[4])))
            # end_pt = (int(float(r[7])), int(float(r[8])))
            # img = cv2.rectangle(img, start_pt, end_pt, color, 2)
            pt1 = [int(float(r[3])), int(float(r[4]))]
            pt2 = [int(float(r[5])), int(float(r[6]))]
            pt3 = [int(float(r[7])), int(float(r[8]))]
            pt4 = [int(float(r[9])), int(float(r[10]))]
            pts = np.asarray([pt1, pt2, pt3, pt4], np.int32)

            img = cv2.polylines(img, [pts], True, color, 2)
            # img = cv2.putText(img, str(pred_class), pt1, cv2.FONT_HERSHEY_SIMPLEX,
            #                     0.5, (0, 255, 0), 1)

    cv2.imwrite(os.path.join("/ws/data/output_img", img_name), img)
    # cv2.imshow('test', img)
    # cv2.waitKey(0)


