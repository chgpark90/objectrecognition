#!/bin/bash

NAME=cpark90/dacon
DATAPATH=/media/data2
BASELINE=/home/cpark/git/ob
DIRNAME=$(cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P)

docker run -it --rm --runtime=nvidia --ipc=host -e DISPLAY=:19 -e QT_X11_NO_MITSHM=1 -v /tmp/.X11-unix:/tmp/.X11-unix -v $DATAPATH:/ws/data -v $BASELINE:/ws/external $NAME
