import logging

from collections import namedtuple
import datetime

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

Announcements = namedtuple('Announcements', 'due_date papers about_data rules')

Announcements.due_date = [2020, 3, 20, 23, 59, 59, 0]
Announcements.papers = open('announcements/papers').read()
Announcements.about_data = open('announcements/about_data').read()
Announcements.rules = open('announcements/rules').read()

def show_inform(update, context):
    msg = 'About commands\n' +\
        '/papers : Show related works URL.\n' +\
        '/about_data : Show rules about dataset.\n' +\
        '/date : Show due, now, remain date.\n' +\
        '/rules : Show about entire rules.'
    update.message.reply_text(msg)

def show_papers(update, context):
    update.message.reply_text(Announcements.papers)

def about_data(update, context):
    update.message.reply_text(Announcements.about_data)

def show_rules(update, context):
    update.message.reply_text(Announcements.rules)

def show_date(update, context):
    due_date = datetime.datetime(*Announcements.due_date)
    now_date = datetime.datetime.now()
    remain_date = due_date - now_date

    msg = due_date.strftime('Due date: %Y, %m, %d\n')
    msg = msg + now_date.strftime('Now: %Y, %m, %d\n')
    msg = msg + 'Remain days: ' + str(remain_date.days)
    update.message.reply_text(msg)

def error(update, context):
    logger.warning('Update "%s caused error "%s"', update, context.error)

def main():
    token_file = open('token', 'r')
    token = token_file.read().splitlines()
    updater = Updater(token[0], use_context=True)

    dp = updater.dispatcher

    dp.add_handler(CommandHandler("help", show_inform))
    dp.add_handler(CommandHandler("papers", show_papers))
    dp.add_handler(CommandHandler("about_data", about_data))
    dp.add_handler(CommandHandler("rules", show_rules))
    dp.add_handler(CommandHandler("date", show_date))

    dp.add_error_handler(error)

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
