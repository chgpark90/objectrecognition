#!/bin/bash
GPU_USAGES=$(nvidia-smi |grep -A1 "TITAN Xp" |awk 'NR % 3 == 2' | awk '{split($0,a,"|"); print a[3]}' | awk '{split($0,a,"/"); print a[1]}' | awk '{print ($0+0)}')
my_array=($(echo $GPU_USAGES | tr " " "\n"))

COUNTER=0
while [ $COUNTER -lt $# ]
do
	let COUNTER++
done

while [ ${my_array[0]} -lt 100 ]
do
	echo ${my_array[0]}
	sleep 1
done
